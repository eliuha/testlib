import os
import sys
from setuptools import setup
from setuptools.command.test import test as TestCommand

here = os.path.abspath(os.path.dirname(__file__))

about = {}
with open(os.path.join(here, 'testlib', '__version__.py'), mode='r', encoding='utf-8') as f:
    exec(f.read(), about)

requires = [
    'requests>=2.23.0'
]
test_requirements = [
    'pytest>=3'
]


def run_tests(self):
    import pytest

    errno = pytest.main(self.pytest_args)
    sys.exit(errno)


setup(
    name=about['__title__'],
    version=about['__version__'],
    description='test git deploy issues',
    url="https://gitlab.com/eliuha/testlib",
    author=about['__author__'],
    author_email=about['__author_email__'],
    packages=['testlib'],
    install_requires=requires,  # external packages as dependencies
    tests_require=test_requirements,
)

print(f"installed [{about['__version__']}]")
