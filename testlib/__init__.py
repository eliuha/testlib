import os

about = {}
here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, '__version__.py'), mode='r', encoding='utf-8') as f:
    exec(f.read(), about)


def it_works():
    print('It Works')
    print(f'path = [{os.getcwd()}]')
    print(f'Absolute path [{os.path.abspath(__file__)}] ')
    return True


def print_version():
    print(f"Deployed version {about['__version__']}")
